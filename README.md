# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

This repo is for phone validation and is based on libphonenumber with some fetchr change

### What Change ###

https://github.com/soujisama/libphonenumber-for-php

-    ‘NationalNumberPattern’ => ’(?:5(?:[013-689]\\d|7[0-36-8])|811\\d)\\d{6}’,
+    ‘NationalNumberPattern’ => ’(?:5(?:[0123-6789]\\d|7[0-36-8])|811\\d)\\d{6}’,

### How to change ###

https://github.com/googlei18n/libphonenumber/blob/master/making-metadata-changes.md

resources/PhoneNumberMetadata.xml, line-22820

013-689 -> 0123-6789

ant -f java/build.xml junit
ant -f java/build.xml jar

### Who do I talk to? ###

Xinyu Ge
Danish Kamal
Oluwasoji Sanyaolu